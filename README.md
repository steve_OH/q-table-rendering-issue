# Quasar QTable rendering issue

This is a minimal Quasar project that demonstrates a bizarre font rendering
issue that appears in Windows 10 with all tested browsers (Chrome, Firefox,
Edge), and with any font. From the looks of it, the problem may be related to
Microsoft's ClearType subpixel antialiasing system, but in any case, there
doesn't seem to be any obvious rationale for why it occurs when it occurs.

## The problem in a nutshell

The appearance of text rendered in a QTable depends on whether or not a
horizontal scrollbar is present.

## The problem in more detail

Here is a screenshot of the app:

![App screenshot](public/screenshot.png)

This shows text rendered under four conditions:

- In ordinary divs that are wide enough to show the text without truncation
- In a q-table that's wide enough to show the text without truncation
- In ordinary divs that are too narrow to show the text without truncation
- In a q-table that's too narrow to show the text without truncation

The appearance of the text is identical in the first three cases. But in the
fourth case, the text is subtly different in color. If we zoom in, we can see
what's going on. Here's the "normal" view, the one seen in the majority of the
cases (and also what's seen in a straight HTML, no Quasar, page):

![Normal rendering](public/normal.png)

This shows the characteristic color fringes that arise from the application of
the ClearType algorithm.

Now here's the outlier:

![QTable with scrollbar](public/q-table-with-scrollbar.png)

The ClearType antialiasing is gone, replaced with simpler pixel-level
antialiasing.

Bear in mind that the **only** difference here is whether or not the scrollbar
is present! If you tweak the app so that the scrollbar comes and goes as you
change the width of the window, you can see the color change dynamically.

I have not been able to reproduce this in a non-Quasar HTML web page (using
&lt;table&gt; elements, etc.), so there's something specific to the way that
Quasar renders the table that's causing it.

Also note that the problem is there regardless of the color of the text; it's
just more noticeable with green text.
